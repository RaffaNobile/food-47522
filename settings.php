<!DOCTYPE html>
<html lang="it">
   <head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=0">
      <title>Cesena Food</title>
      <?php include 'include.php'; ?>
      <link rel="stylesheet" href="JS/CSS/style.css">
   </head>
<body>
  <div id="container">
    <div id="main">
      <?php include 'PHP/db_connect.php';?>
      <?php include 'PHP/functions.php';?>
      <?php
      sec_session_start();
      if(login_check($conn) == true) {?>
      <?php include 'PHP/cart.php'; ?>
      <?php include 'PHP/header.php'; ?>
      <?php include 'PHP/hamburger.php'; ?>
      <div class="container container-register">
        <?php include 'PHP/cart-button.php'?>


          <div class="margin50"><h1 class="text-center display-4 subtitle">Gestione Account</h1></div>

          <button type="button" class="btn btn-primary margin20" onclick="location.href='./account.php'"><i class="material-icons" style="vertical-align: sub; font-size: 20px;">chevron_left</i>Indietro</button>
          <?php
          $user_id = $_SESSION['id'];
          if ($stmt = $conn->prepare("SELECT username, address, phone FROM users WHERE id = ?")) {
            $stmt->bind_param('i', $user_id);
            $stmt->execute(); // esegue la query appena creata.
            $stmt->store_result();
            $stmt->bind_result($username, $address, $phone); // recupera il risultato della query e lo memorizza nelle relative variabili.
            $stmt->fetch()
            ?>
              <div class="card border-secondary w-100 margin20">
                <ul class="list-group list-group-flush" style="margin: 0px;">
                  <li class="list-group-item">
                    <div class="settings-text">
                      <h6 class="no-margin">
                        Username:
                      </h6>
                      <p class="no-margin">
                        <?php echo $username;?>
                      </p>
                    </div>
                    <div class="settings-btn">
                      <button type="button" class="btn btn-primary margin20" onclick="location.href='./settings-changeusr.php'">Modifica</button>
                    </div>
                  </li>
                  <li class="list-group-item">
                    <div class="settings-text">
                      <h6 class="no-margin">
                        Password:
                      </h6>
                      <p class="no-margin">
                        **********
                      </p>
                    </div>
                    <div class="settings-btn">
                      <button type="button" class="btn btn-primary margin20" onclick="location.href='./settings-changepsw.php'">Modifica</button>
                    </div>
                  </li>
                  <li class="list-group-item">
                    <div class="settings-text">
                      <h6 class="no-margin">
                        Indirizzo:
                      </h6>
                      <p class="no-margin">
                        <?php echo $address;?>
                      </p>
                    </div>
                    <div class="settings-btn">
                      <button type="button" class="btn btn-primary margin20" onclick="location.href='./settings-changeadr.php'">Modifica</button>
                    </div>
                  </li>
                  <li class="list-group-item">
                    <div class="settings-text">
                      <h6 class="no-margin">
                        Telefono:
                      </h6>
                      <p class="no-margin">
                        <?php echo $phone;?>
                      </p>
                    </div>
                    <div class="settings-btn">
                      <button type="button" class="btn btn-primary margin20" onclick="location.href='./settings-changephn.php'">Modifica</button>
                    </div>
                  </li>
                </ul>
              </div>
          <?php
          }
        } else {?>
          <div class="alert alert-danger" role="alert">
            <p>Non sei autorizzato ad accedere a questa pagina! Per favore <a href="login.php">accedi</a> prima di procedere.</p>
          </div>
        <?php } ?>

      </div>
    </div>
  </div>

    <div class="lightbox js-lightbox js-toggle-cart"></div>
    <?php include'PHP/footer.php' ?>

    <script src="JS/menu.js"></script>
    <script src="JS/cart.js"></script>

</body>
</html>

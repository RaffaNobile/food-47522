<!DOCTYPE html>
<?php
   $currency = '	&euro; ';
   $shipping_cost = 1.50;
    ?>
<html lang="it">
   <head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=0">
      <title>City Food</title>
      <?php include 'include.php' ?>
      <link rel="stylesheet" href="JS/CSS/style.css">
   </head>
   <body id="main-page">
      <?php include 'PHP/db_connect.php';?>
      <?php include 'PHP/functions.php';?>
      <?php
         sec_session_start();
         ?>
      <?php include 'PHP/cart.php' ?>
      <?php include 'PHP/header.php' ?>
      <?php include 'PHP/hamburger.php' ?>
      <div class="lightbox js-lightbox js-toggle-cart"></div>
      <div id="container">
         <div id="main">
            <?php if(login_check($conn) == false) { ?>
            <div class="alert alert-danger text-center" role="alert" id="checkout-alert">
               <p>Non sei autorizzato ad accedere a questa pagina! Per favore <a href="signup.php">registrati</a> o <a href="login.php">accedi</a> prima di procedere.</p>
            </div>
            <?php
               } else {?>
            <div class="container container-register" id = "checkout-container">
               <h3 style="text-align:left">Controlla il carrello prima di effettuare l'ordine</h3>
               <?php
                  if(isset($_SESSION["products"]) && count($_SESSION["products"])>0){
                  	$total = 0;
                  	?>
               <table class="table-bordered" id="shopping-cart-results">
                  <thead>
                     <tr>
                        <th>Prodotto</th>
                        <th>Prezzo</th>
                        <th>Quantità</th>
                        <th>Costo</th>
                     </tr>
                  </thead>
                  <tbody>
                     <?php
                        $cart_box = '';
                        foreach($_SESSION["products"] as $product){
                        	$product_name = $product["name"];
                        	$product_qty = $product["product_qty"];
                        	$product_price = $product["price"];
                        	$product_code = $product["product_code"];
                        	$item_price = sprintf("%01.2f",($product_price * $product_qty));
                        	?>
                     <tr>
                        <td><?php echo $product_name; ?></td>
                        <td style="text-align:center"><?php echo $currency ; echo " "; echo $product_price; ?></td>
                        <td><input type="number" data-code="<?php echo $product_code; ?>" class="form-control text-center quantity" value="<?php echo $product_qty; ?>" ></td>
                        <td><?php echo $currency; echo sprintf("%01.2f", ($product_price * $product_qty)); ?></td>
                     </tr>
										 <div class="total-cost">
											 <?php
	                        $subtotal = ($product_price * $product_qty);
	                        $total = ($total + $subtotal);
	                        }
	                        $grand_total = $total + $shipping_cost;

	                        $shipping_cost = ($shipping_cost)?'Costo consegna : '.$currency. sprintf("%01.2f", $shipping_cost).'<br />':'';
	                        $cart_box .= "<span>$shipping_cost  <hr>Totale : $currency ".sprintf("%01.2f", $grand_total)."</span>";
	                        ?>

											</div>
										</tbody>
								 </table>

								 <div class="card">
                   <div class="text-center card-body" id="total-checkout">
                     <p class="textview-cart-total"><strong><?php echo $cart_box; ?></strong></p>
    </div>
    </div>

								 <div class="checkout-buttons">
									 <a type="button" href="index.php" class="btn btn-warning" id ="back-home">Torna alla home!</a>
									 <a type="button" href="payment.php" class="btn btn-success">Procedi!</i></a>
								 </div>
							 </div>
							 <?php
									} else {
									 echo "Il carrello è vuoto";
									}}
									?>
						</div>
				 </div>
      <script src="JS/menu.js"></script>
      <script src="JS/cart.js"></script>
      <?php include 'PHP/footer.php' ?>
   </body>
</html>

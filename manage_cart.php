<?php
$currency = '	&euro; ';
include_once("PHP/db_connect.php");
include_once("PHP/functions.php");
sec_session_start();

setlocale(LC_MONETARY,"it_IT");
# add products in cart
if(isset($_POST["product_code"])) {
	foreach($_POST as $key => $value){
		$product[$key] = filter_var($value, FILTER_SANITIZE_STRING);
	}
	$statement = $conn->prepare("SELECT name, price, id FROM food WHERE product_code=? LIMIT 1");
	$statement->bind_param('s', $product['product_code']);
	$statement->execute();
	$statement->bind_result($product_name, $product_price, $product_id);

	while($statement->fetch()){
		$product["name"] = $product_name;
		$product["price"] = $product_price;
		if(isset($_SESSION["products"])){
			if(isset($_SESSION["products"][$product['product_code']])) {
				$_SESSION["products"][$product['product_code']]["product_qty"] = $_SESSION["products"][$product['product_code']]["product_qty"] + $_POST["product_qty"];
			} else {
				$_SESSION["products"][$product['product_code']] = $product;
			}
		} else {
			$_SESSION["products"][$product['product_code']] = $product;
		}
	}
 	$total_product = count($_SESSION["products"]);
	die(json_encode(array('products'=>$total_product)));
}
# Remove products from cart
if(isset($_GET["remove_code"]) && isset($_SESSION["products"])) {
	$product_code  = filter_var($_GET["remove_code"], FILTER_SANITIZE_STRING);
	if(isset($_SESSION["products"][$product_code]))	{
		unset($_SESSION["products"][$product_code]);
	}
 	$total_product = count($_SESSION["products"]);
	die(json_encode(array('products'=>$total_product)));
}
# Update cart product quantity
if(isset($_GET["update_quantity"]) && isset($_SESSION["products"])) {
	if(isset($_GET["quantity"]) && $_GET["quantity"]>0) {
		$_SESSION["products"][$_GET["update_quantity"]]["product_qty"] = $_GET["quantity"];
	}
	$total_product = count($_SESSION["products"]);
	die(json_encode(array('products'=>$total_product)));
}
?>

<div class="container" id="view_cart">
<?php
// view_cart
if(isset($_POST["load_cart"]) && $_POST["load_cart"]==1)
{
	if(isset($_SESSION["products"]) && count($_SESSION["products"])>0){ //if we have session variable
    ?>

    <table class="table-striped" id="tavola-risultati">
    <thead>
    <tr>
    <th>Prodotti</th>
    <th>Quantità</th>
    <th>Costo</th>
    </tr>
    </thead>
    <tbody>
  <?php

    $cart_box = '<ul class="cart-products-loaded">';
    $total = 0;

    foreach($_SESSION["products"] as $product){

      $product_name = $product["name"];
      $product_price = $product["price"];
      $product_code = $product["product_code"];
      $product_qty = $product["product_qty"];
      $subtotal = ($product_price * $product_qty);
      $total = ($total + $subtotal);
    ?>
    <tr>
    <td><?php echo $product_name;  ?></td>

    <td><input type="text" data-code="<?php echo $product_code; ?>" class="form-control text-center quantity" value="<?php  echo $product_qty;?>" readonly></td>
    <td><?php echo $currency; echo sprintf("%01.2f", ($product_price * $product_qty)); ?></td>
    <td style="text-align:center">
    <a href="#" class="btn btn-danger remove-item" data-code="<?php echo $product_code; ?>"><i class="fa fa-trash-o"></i></a>
    </td>
    </tr>
   <?php } ?>
  <tfoot>
  <br>
  <br>
  <tr>
  <td colspan="2"></td>

  </tr>
  </tfoot>
  <?php
  } else {
    echo "Il carrello è vuoto";
  ?>
  <tfoot>
  <br>
  <br>
  <tr>
  <td colspan="2"></td>
  </tr>
  </tfoot>
<?php } }?>
  </tbody>
  </table>
  <?php
 if(isset($total)) {
 ?>
 <p class="text-center cart-products-total"><strong>Totale: <?php echo $currency.sprintf("%01.2f",$total); ?></strong></p>
 <div class="checkout-btn">
    <a href="checkout.php" class="btn btn-success btn-block">Acquista<i class="glyphicon glyphicon-menu-right"></i></a>
 </div>

 <?php } ?>
</div>

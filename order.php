<!DOCTYPE html>
<html lang="it">
   <head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=0">
      <title>Cesena Food</title>
      <?php include 'include.php'; ?>
      <?php include 'PHP/db_connect.php';?>
      <?php include 'PHP/functions.php';?>
      <?php sec_session_start();
            if (isset($_GET['order_id'])) {
              $order_id = $_GET['order_id'];
              if (isset($_POST['ordinericevuto'])) {
                if ($insert_stmt = $conn->prepare("UPDATE orders SET status = 0 WHERE id = $order_id")) {
                  // Esegui la query ottenuta.
                  $insert_stmt->execute();
                  $insert_stmt->close();
                }
              }
              if (isset($_POST['inconsegna'])) {
                if ($insert_stmt = $conn->prepare("UPDATE orders SET status = 1 WHERE id = $order_id")) {
                  // Esegui la query ottenuta.
                  $insert_stmt->execute();
                  $insert_stmt->close();
                }
              }
              if (isset($_POST['consegnato'])) {
                if ($insert_stmt = $conn->prepare("UPDATE orders SET status = 2 WHERE id = $order_id")) {
                  // Esegui la query ottenuta.
                  $insert_stmt->execute();
                  $insert_stmt->close();
                }
              }
            }
      ?>
      <link rel="stylesheet" href="JS/CSS/style.css">
   </head>
<body>


  <div id="container">
    <div id="main">

      <?php if(login_check($conn) == true) {?>
      <?php include 'PHP/cart.php'; ?>
      <?php include 'PHP/header.php'; ?>
      <?php include 'PHP/hamburger.php'; ?>

          <div class="container container-register">
            <?php include 'PHP/cart-button.php'?>

              <div class="margin50"><h1 class="text-center display-4 subtitle">Gestione Account</h1></div>
              <button type="button" class="btn btn-primary margin20" onclick="location.href='./account.php'"><i class="material-icons" style="vertical-align: sub; font-size: 20px;">chevron_left</i>Indietro</button>
              <?php
              $user_id = $_SESSION['id'];
              if ($stmt = $conn->prepare("SELECT admin FROM users WHERE id = ?")) {
                $stmt->bind_param('i', $user_id);
                $stmt->execute(); // esegue la query appena creata.
                $stmt->store_result();
                $stmt->bind_result($admin); // recupera il risultato della query e lo memorizza nelle relative variabili.
                $stmt->fetch();
                $stmt->close();
                if ($admin == 0) {
                  if ($stmt = $conn->prepare("SELECT price, date, address, status FROM orders WHERE user_id = ? AND id = ?")) {
                    $stmt->bind_param('ii', $user_id, $order_id);
                    $stmt->execute(); // esegue la query appena creata.
                    $stmt->store_result();
                    $stmt->bind_result($totprice, $date, $address, $status); // recupera il risultato della query e lo memorizza nelle relative variabili.
                    if ($stmt->fetch()) {
                      $date = strtotime($date);
                      $data = date('d/m/Y', $date);
                      $tempo = date('H:i',$date);
                      if ($stmt = $conn->prepare("SELECT food.name, details.price, details.quantity FROM orders, details INNER JOIN food ON details.food_id = food.id WHERE orders.user_id = ? AND orders.id = ? AND orders.id = details.order_id")) {
                        $stmt->bind_param('ii', $user_id, $order_id);
                        $stmt->execute(); // esegue la query appena creata.
                        $stmt->store_result();
                        $stmt->bind_result($name, $price, $quantity); // recupera il risultato della query e lo memorizza nelle relative variabili.
                    ?>
                      <div class="card border-secondary w-100 margin20">
                        <div class="card-body" style="padding-bottom: 0px;">
                          <?php
                            switch ($status) {
                              case '1':
                                echo '<div class="alert alert-primary" role="alert">In consegna</div></li>';
                                break;
                              case '2':
                                echo '<div class="alert alert-success" role="alert">Consegnato</div></li>';
                                break;
                              default:
                                echo '<div class="alert alert-secondary" role="alert">Ordine ricevuto</div></li>';
                                break;
                            }
                          ?>
                            <table class="table table-sm">
                              <thead>
                                <tr>
                                  <th scope="col">Articolo</th>
                                  <th scope="col">Quanità</th>
                                  <th scope="col">Prezzo parziale</th>
                                </tr>
                              </thead>
                              <tbody>
                                <?php
                                  $c = 0;
                                  while ($stmt->fetch()) {
                                    $c += $price*$quantity;
                                    echo "<tr><td>$name</td><td>$quantity</td><td>"; echo $price*$quantity; echo " €</td></tr>";
                                  }
                                  echo "<tr><td></td><td>Tot</td><td>"; echo $c; echo " €</td></tr>";
                                ?>
                              </tbody>
                            </table>
                        </div>
                        <ul class="list-group list-group-flush">
                          <li class="list-group-item"><?php
                          switch ($status) {
                            case '2':
                              echo "L&#39;ordine è stato consegnato presso:<br/>$address";
                              break;
                            case '1':
                              echo "L&#39;ordine è in consegna presso:<br/>$address";
                              break;
                            default:
                              echo "L&#39;ordine verrà consegnato presso:<br/>$address";
                              break;
                          }
                          ?>
                          </li>
                          <li class="list-group-item"><?php
                            echo "Ordine effettuato in data:<br/>$data, $tempo";
                          ?>
                          </li>
                        </ul>
                      </div>
                  <?php
                      }
                    }
                    else {
                      echo '<div class="alert alert-danger" role="alert">
                            L&#39;ordine non esiste!<br/>Ritorna alla pagina dell&#39;<a href="account.php">account</a>.
                            </div>';
                    }
                  }
                } else {
                  if ($stmt = $conn->prepare("SELECT price, date, address, status FROM orders WHERE id = ?")) {
                    $stmt->bind_param('i', $order_id);
                    $stmt->execute(); // esegue la query appena creata.
                    $stmt->store_result();
                    $stmt->bind_result($totprice, $date, $address, $status); // recupera il risultato della query e lo memorizza nelle relative variabili.
                    if ($stmt->fetch()) {
                      $date = strtotime($date);
                      $data = date('d/m/Y', $date);
                      $tempo = date('H:i',$date);
                      if ($stmt = $conn->prepare("SELECT food.name, details.price, details.quantity FROM orders, details INNER JOIN food ON details.food_id = food.id WHERE orders.user_id = ? AND orders.id = ? AND orders.id = details.order_id")) {
                        $stmt->bind_param('ii', $user_id, $order_id);
                        $stmt->execute(); // esegue la query appena creata.
                        $stmt->store_result();
                        $stmt->bind_result($name, $price, $quantity); // recupera il risultato della query e lo memorizza nelle relative variabili.
                    ?>
                      <div class="card border-secondary w-100 margin20">
                        <div class="card-body" style="padding-bottom: 0px;">
                          <?php
                            switch ($status) {
                              case '1':
                                echo '<div class="alert alert-primary" role="alert"><div class="btn-group">
                                        <div class="btn-group">
                                          <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            In consegna
                                          </button>';
                                break;
                              case '2':
                              echo '<div class="alert alert-success" role="alert"><div class="btn-group">
                                      <div class="btn-group">
                                        <button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                          Consegnato
                                        </button>';
                                break;
                              default:
                              echo '<div class="alert alert-secondary" role="alert"><div class="btn-group">
                                      <div class="btn-group">
                                        <button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                          Ordine Ricevuto
                                        </button>';
                                break;
                            }
                          ?>
                                  <div class="dropdown-menu">
                                    <?php echo "<form action='order.php?order_id=$order_id' method='post'>" ?>
                                      <input type="submit" class="dropdown-item" name="ordinericevuto" value="Ordine Ricevuto"/>
                                      <input type="submit" class="dropdown-item" name="inconsegna" value="In Consegna"/>
                                      <input type="submit" class="dropdown-item" name="consegnato" value="Consegnato"/>
                                    </form>
                                  </div>
                                </div>
                              </div></div>
                            <table class="table table-sm">
                              <thead>
                                <tr>
                                  <th scope="col">Articolo</th>
                                  <th scope="col">Quanità</th>
                                  <th scope="col">Prezzo parziale</th>
                                </tr>
                              </thead>
                              <tbody>
                                <?php
                                  $c = 0;
                                  while ($stmt->fetch()) {
                                    $c += $price*$quantity;
                                    echo "<tr><td>$name</td><td>$quantity</td><td>"; echo $price*$quantity; echo " €</td></tr>";
                                  }
                                  echo "<tr><td></td><td>Tot</td><td>"; echo $c; echo " €</td></tr>";
                                ?>
                              </tbody>
                            </table>
                        </div>
                        <ul class="list-group list-group-flush">
                          <li class="list-group-item"><?php
                          switch ($status) {
                            case '2':
                              echo "L&#39;ordine è stato consegnato presso:<br/>$address";
                              break;
                            case '1':
                              echo "L&#39;ordine è in consegna presso:<br/>$address";
                              break;
                            default:
                              echo "L&#39;ordine verrà consegnato presso:<br/>$address";
                              break;
                          }
                          ?>
                          </li>
                          <li class="list-group-item"><?php
                            echo "Ordine effettuato in data:<br/>$data, $tempo";
                          ?>
                          </li>
                        </ul>
                      </div>
                  <?php
                      }
                    }
                    else {
                      echo '<div class="alert alert-danger" role="alert">
                            L&#39;ordine non esiste!<br/>Ritorna alla pagina dell&#39;<a href="account.php">account</a>.
                            </div>';
                    }
                  }
                }
              }
            } else {?>
              <div class="alert alert-danger" role="alert">
                <p>Non sei autorizzato ad accedere a questa pagina! Per favore <a href="login.php">accedi</a> prima di procedere.</p>
              </div>
            <?php } ?>

          </div>

    </div>
  </div>


    <div class="lightbox js-lightbox js-toggle-cart"></div>
    <?php include'PHP/footer.php' ?>

    <script src="JS/menu.js"></script>
    <script src="JS/cart.js"></script>

</body>
</html>

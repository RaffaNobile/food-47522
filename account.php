<!DOCTYPE html>
<html lang="it">
   <head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=0">
      <title>Cesena Food</title>
      <?php include 'include.php'; ?>
      <link rel="stylesheet" href="JS/CSS/style.css">
   </head>
<body>

    <div id="container">
      <div id="main">

          <?php
          include 'PHP/db_connect.php';
          include 'PHP/functions.php';
          sec_session_start();
          include 'PHP/cart.php';
          include 'PHP/header.php';
          include 'PHP/hamburger.php';
          if(login_check($conn) == true) {?>
            <div class="container container-register">
              <?php include 'PHP/cart-button.php'?>
                <div class="margin50"><h1 class="text-center display-4 subtitle">Gestione Account </h1>
                  <button type="button" class="btn btn-primary btn-orders" onclick="location.href='./settings.php'"><i class="material-icons" style="vertical-align: sub;">settings</i> Impostazioni</button>
                </div>


                <h1 class="display-4" style="font-size: 1.5rem;">Ordini</h1>

                <?php
                $user_id = $_SESSION['id'];
                if ($stmt = $conn->prepare("SELECT admin FROM users WHERE id = ?")) {
                  $stmt->bind_param('i', $user_id);
                  $stmt->execute(); // esegue la query appena creata.
                  $stmt->store_result();
                  $stmt->bind_result($admin); // recupera il risultato della query e lo memorizza nelle relative variabili.
                  $stmt->fetch();
                  $stmt->close();
                  if ($admin == 0) {
                    if ($stmt = $conn->prepare("SELECT id, price, date, address, status FROM orders WHERE user_id = ? ORDER BY date DESC")) {
                      $lista_vuota = true;
                      $stmt->bind_param('i', $user_id);
                      $stmt->execute(); // esegue la query appena creata.
                      $stmt->store_result();
                      $stmt->bind_result($id, $price, $date, $address, $status); // recupera il risultato della query e lo memorizza nelle relative variabili.
                      while ($stmt->fetch()) {
                        $lista_vuota = false;
                        $date = strtotime($date);
                        $data = date('d/m/Y', $date);
                        $tempo = date('H:i',$date);
                      ?>
                      <a href="order.php?order_id=<?php echo $id;?>" class="linkordini">
                        <div class="card border-secondary w-100 margin20">
                          <div class="card-body" style="padding-bottom: 0px;">
                            <h5 class="card-title">Ordine del <?php echo "$data, $tempo";?></h5>
                            <p class="card-text"><?php echo $address;?></p>
                          </div>
                          <ul class="list-group list-group-flush">
                            <li class="list-group-item"><p class="order"><?php echo "$price €";?></p>
                            <?php
                              switch ($status) {
                                case '1':
                                  echo '<div class="alert alert-primary alert-order" role="alert">In consegna</div></li>';
                                  break;
                                case '2':
                                  echo '<div class="alert alert-success alert-order" role="alert">Consegnato</div></li>';
                                  break;
                                default:
                                  echo '<div class="alert alert-secondary alert-order" role="alert">Ordine ricevuto</div></li>';
                                  break;
                              }
                            ?>
                          </ul>
                        </div>
                      </a>
                    <?php
                      }
                      if ($lista_vuota) {
                        echo '<div class="alert alert-primary" role="alert">
                              Non hai ancora effettuato alcun ordine
                              </div>';
                      }
                    }
                  } else {
                    if ($insert_stmt = $conn->prepare("UPDATE orders SET seen = 1 WHERE seen = 0")) {
                  		// Esegui la query ottenuta.
                  		$insert_stmt->execute();
                      $insert_stmt->close();
                    }
                    if ($stmt = $conn->prepare("SELECT id, price, date, address, status FROM orders ORDER BY date DESC")) {
                      $lista_vuota = true;
                      $stmt->execute(); // esegue la query appena creata.
                      $stmt->store_result();
                      $stmt->bind_result($id, $price, $date, $address, $status); // recupera il risultato della query e lo memorizza nelle relative variabili.
                      while ($stmt->fetch()) {
                        $lista_vuota = false;
                        $date = strtotime($date);
                        $data = date('d/m/Y', $date);
                        $tempo = date('H:i',$date);
                      ?>
                      <a href="order.php?order_id=<?php echo $id;?>" class="linkordini">
                        <div class="card border-secondary w-100 margin20">
                          <div class="card-body" style="padding-bottom: 0px;">
                            <h5 class="card-title">Ordine del <?php echo "$data, $tempo";?></h5>
                            <p class="card-text"><?php echo $address;?></p>
                          </div>
                          <ul class="list-group list-group-flush">
                            <li class="list-group-item"><p class="order"><?php echo "$price €";?></p>
                            <?php
                              switch ($status) {
                                case '1':
                                  echo '<div class="alert alert-primary alert-order" role="alert">In consegna</div></li>';
                                  break;
                                case '2':
                                  echo '<div class="alert alert-success alert-order" role="alert">Consegnato</div></li>';
                                  break;
                                default:
                                  echo '<div class="alert alert-secondary alert-order" role="alert">Ordine ricevuto</div></li>';
                                  break;
                              }
                            ?>
                          </ul>
                        </div>
                      </a>
                    <?php
                      }
                      if ($lista_vuota) {
                        echo '<div class="alert alert-primary" role="alert">
                              Non hai ancora effettuato alcun ordine
                              </div>';
                      }
                    }
                  }
                }
              } else {?>
                <div class="alert alert-danger" role="alert" id="user-alert">
                  <p>Non sei autorizzato ad accedere a questa pagina! Per favore <a href="login.php">accedi</a> prima di procedere.</p>
                </div>
              <?php } ?>
      </div>
    </div>
    </div>

    <div class="lightbox js-lightbox js-toggle-cart"></div>
    <?php include'PHP/footer.php' ?>

    <script src="JS/menu.js"></script>
    <script src="JS/cart.js"></script>

</body>
</html>

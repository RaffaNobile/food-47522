<!DOCTYPE html>
<html lang="it">
   <head>
     <?php
        include 'PHP/db_connect.php';
        include 'PHP/functions.php';
        sec_session_start();
        $user_id = $_SESSION['id'];
        $reg = 0;
        if (isset($_POST["phone"])) {
        	$phone = $_POST['phone'];
        	if ($insert_stmt = $conn->prepare("UPDATE users SET phone = ? WHERE id = ?")) {
        		$insert_stmt->bind_param('si', $phone, $user_id);
        		// Esegui la query ottenuta.
        		$insert_stmt->execute();

            $reg = 1;
        	}
        }
        ?>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=0">
        <title>Cesena Food</title>
        <?php include 'include.php'; ?>
        <link rel="stylesheet" href="JS/CSS/style.css">
     </head>
   <body>
     <div id="container">
       <div id="main">
         <?php include 'PHP/cart.php'; ?>
         <?php include 'PHP/header.php'; ?>
         <?php include 'PHP/hamburger.php'; ?>
         <section>
               <div class="container container-register">
                 <div class="margin50"><h1 class="text-center display-4 subtitle">Gestione Account</h1></div>
                 <?php
                 if(login_check($conn) == true) {
                 ?>
                 <button type="button" class="btn btn-primary margin20" onclick="location.href='./settings.php'"><i class="material-icons" style="vertical-align: sub; font-size: 20px;">chevron_left</i>Indietro</button>
                  <?php
                     if ($reg) { ?>
                  <div class="alert alert-success" role="alert">
                     Cambio telefono effettuato
                  </div>
                  <?php } ?>
                  <form action="settings-changephn.php" method="post" name="signup_form">
                     <div class="form-row align-items-center">
                        <div class="col-sm-8 my-1">
                           <label for="phone">Nuovo telefono</label>
                           <input type="text" class="form-control" id="phone" name="phone" required/><br />
                        </div>
                     </div>
                     <input type="submit" class="btn btn-primary fullsize" value="Salva" />
                     <!--<input type="submit" class="btn btn-primary fullsize" name="invia" value="Registrati"/>-->
                  </form>
                <?php } else { ?>
                  <div class="alert alert-danger" role="alert">
                    <p>Non sei autorizzato ad accedere a questa pagina! Per favore <a href="login.php">accedi</a> prima di procedere.</p>
                  </div>
                <?php } ?>
               </div>
             </section>

       </div>
     </div>



      <div class="lightbox js-lightbox js-toggle-cart"></div>
      <?php include'PHP/footer.php' ?>

      <script src="JS/menu.js"></script>
      <script src="JS/cart.js"></script>

   </body>
</html>

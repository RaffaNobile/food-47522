<!DOCTYPE html>
<html lang="it">
   <head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=0">
      <title>Cesena Food</title>
      <?php include 'include.php' ?>
      <link rel="stylesheet" href="JS/CSS/style.css">
   </head>
   <body>
      <?php include 'PHP/db_connect.php';?>
      <?php include 'PHP/functions.php';?>
      <?php
         sec_session_start();
         ?>
      <?php include 'PHP/cart.php' ?>
      <?php include 'PHP/header.php' ?>
      <?php include 'PHP/hamburger.php' ?>
      <div class="lightbox js-lightbox js-toggle-cart"></div>
      <div id="container">
         <div id="main">

            <?php include 'PHP/cart-button.php'?>
            <div class="container container-register" id="food-container">
               <?php
                  $result = $conn->query("SELECT category FROM food GROUP BY category");
                  ?>
                  <div class="card-columns" id="card-colonne">
                    <?php
                  while ($row = $result->fetch_assoc()) {
                  ?>

                  <a href="fooddetails.php?category=<?php echo $row["category"];?>">

               <div class="card" style="width: 18rem;" id="card-products">
                   <?php
                     switch ($row["category"]) {
                       case 'Pizze':
                          ?>
                    <img class="card-img-top" src= "JS/CSS/img/pizza.jpg" alt="Card image cap" id="card-img">
                    <div class="overlay"><?php echo $row["category"];?></div>

                     <?php
                        break;
                        case 'Panini':
                        ?>
                    <img class="card-img-top" src= "JS/CSS/img/panino.jpg" alt="Card image cap" id="card-img">
                    <div class="overlay"><?php echo $row["category"];?></div>
                     <?php
                        break;
                        case 'Piade':
                        ?>
                     <img class="card-img-top" src="JS/CSS/img/piada.jpg" alt="Card image cap" id="card-img" >
                     <div class="overlay"><?php echo $row["category"];?></div>
                     <?php
                        break;
                        case 'Sushi':
                        ?>
                     <img class="card-img-top" src="JS/CSS/img/sushi.jpg" alt="Card image cap" id="card-img">
                     <div class="overlay"><?php echo $row["category"];?></div>

                     <?php
                        break;
                        }
                        ?>


               </div>
               </a>

            <?php
               }
               ?>
               </div>
         </div>
      </div>
      </div>


      <script src="JS/menu.js"></script>
      <script src="JS/cart.js"></script>
      <?php include 'PHP/footer.php' ?>
   </body>
</html>

-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Creato il: Lug 01, 2018 alle 17:18
-- Versione del server: 10.1.32-MariaDB
-- Versione PHP: 7.2.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `city_food`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `details`
--

CREATE TABLE `details` (
  `id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `food_id` int(11) NOT NULL,
  `price` double NOT NULL,
  `quantity` varchar(3) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dump dei dati per la tabella `details`
--

INSERT INTO `details` (`id`, `order_id`, `food_id`, `price`, `quantity`) VALUES
(1, 1, 1, 5, '2'),
(2, 1, 7, 7, '2'),
(3, 5, 0, 5, '4'),
(4, 5, 0, 8, '1'),
(5, 6, 0, 4, '2'),
(6, 6, 0, 5, '2'),
(7, 6, 0, 3, '1'),
(8, 7, 7, 5, '8'),
(9, 8, 9, 3, '1'),
(10, 8, 12, 4, '1'),
(11, 9, 9, 3, '1'),
(12, 9, 12, 4, '1'),
(13, 10, 12, 4, '1'),
(14, 10, 9, 3, '1'),
(15, 11, 7, 5, '1');

-- --------------------------------------------------------

--
-- Struttura della tabella `food`
--

CREATE TABLE `food` (
  `id` int(11) NOT NULL,
  `name` varchar(60) COLLATE utf8_bin NOT NULL,
  `category` varchar(60) COLLATE utf8_bin NOT NULL,
  `price` double NOT NULL,
  `description` varchar(256) COLLATE utf8_bin NOT NULL,
  `product_code` varchar(60) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dump dei dati per la tabella `food`
--

INSERT INTO `food` (`id`, `name`, `category`, `price`, `description`, `product_code`) VALUES
(1, 'Margherita', 'Pizze', 5, 'Pomodoro, Mozzarella', 'P1'),
(2, 'Diavola', 'Pizze', 7, 'Pomodoro, Mozzarella, Salame piccante', 'P2'),
(7, 'Porky', 'Panini', 5, 'Pane, Porchetta', 'Pa1'),
(8, 'Maki', 'Sushi', 8, 'Maki misti (8 pezzi)', 'S1'),
(9, 'Crudo', 'Piade', 3, 'Piadina allo strutto, Prosciutto crudo', 'R1'),
(10, 'Uramaki', 'Sushi', 10, 'Uramaki misti (8 pezzi)', 'S2'),
(11, 'Temaki', 'Sushi', 8, 'Temaki misti (3 pezzi)', 'S3'),
(12, 'Salsiccia', 'Piade', 4, 'Piadina allo strutto, Salsiccia', 'R2');

-- --------------------------------------------------------

--
-- Struttura della tabella `login_attempts`
--

CREATE TABLE `login_attempts` (
  `user_id` int(11) NOT NULL,
  `time` varchar(30) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dump dei dati per la tabella `login_attempts`
--

INSERT INTO `login_attempts` (`user_id`, `time`) VALUES
(7, '1526282203'),
(9, '1526500343'),
(8, '1530053029');

-- --------------------------------------------------------

--
-- Struttura della tabella `orders`
--

CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `price` varchar(128) COLLATE utf8_bin NOT NULL,
  `date` datetime NOT NULL,
  `address` varchar(200) COLLATE utf8_bin NOT NULL,
  `seen` tinyint(1) NOT NULL,
  `status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dump dei dati per la tabella `orders`
--

INSERT INTO `orders` (`id`, `user_id`, `price`, `date`, `address`, `seen`, `status`) VALUES
(11, 12, '6.5', '2018-07-01 17:15:32', 'Via Lugano 34', 1, 2);

-- --------------------------------------------------------

--
-- Struttura della tabella `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(30) COLLATE utf8_bin NOT NULL,
  `email` varchar(50) COLLATE utf8_bin NOT NULL,
  `password` char(128) COLLATE utf8_bin NOT NULL,
  `salt` char(128) COLLATE utf8_bin NOT NULL,
  `address` varchar(200) COLLATE utf8_bin NOT NULL,
  `name` varchar(30) COLLATE utf8_bin NOT NULL,
  `lastname` varchar(50) COLLATE utf8_bin NOT NULL,
  `phone` varchar(20) COLLATE utf8_bin NOT NULL,
  `admin` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dump dei dati per la tabella `users`
--

INSERT INTO `users` (`id`, `username`, `email`, `password`, `salt`, `address`, `name`, `lastname`, `phone`, `admin`) VALUES
(12, 'Admin', 'admin@gmail.it', 'bd5271244ba72e4a2d507d71beae2cc4497d0b119d1762e861862f29b6dcec6640240b7d269f18e5cda43cb8f38c1f1ab853a40b2ca10f103e9228e4b77a1c5d', '54975ee6a2925b52acd8f738236b40416e55ef356e4a786d142e6acc336bb591118892c4f3c7def8b98ebe79be3fbf05dd108f0c8ee98eabaad2cd6a7b157bb5', 'Via Lugano 34', 'Admin', 'Admin', '0541809060', 1);

--
-- Indici per le tabelle scaricate
--

--
-- Indici per le tabelle `details`
--
ALTER TABLE `details`
  ADD PRIMARY KEY (`id`);

--
-- Indici per le tabelle `food`
--
ALTER TABLE `food`
  ADD PRIMARY KEY (`id`);

--
-- Indici per le tabelle `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indici per le tabelle `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT per le tabelle scaricate
--

--
-- AUTO_INCREMENT per la tabella `details`
--
ALTER TABLE `details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT per la tabella `food`
--
ALTER TABLE `food`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT per la tabella `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT per la tabella `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

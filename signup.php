<!DOCTYPE html>
<html lang="it">
   <head>
     <?php
        $reg = 0;
        include 'PHP/db_connect.php';
        include 'PHP/functions.php';
        sec_session_start();

        if (isset($_POST["username"]) and isset($_POST["email"]) and isset($_POST["p"]) and isset($_POST["address"]) and isset($_POST["name"]) and isset($_POST["lastname"]) and isset($_POST["phone"])) {
        	$username = $_POST['username'];
        	$email = $_POST['email'];
          if ($stmt = $conn->prepare("SELECT COUNT(email) FROM users WHERE email = ?")) {
        		$stmt->bind_param('s', $email);
        		// Esegui la query ottenuta.
        		$stmt->execute();
            $stmt->bind_result($email_db);
            $stmt->fetch();
            $stmt->close();
            if ($email_db == 0) {
            	$address = $_POST['address'];
            	$name = $_POST['name'];
            	$lastname = $_POST['lastname'];
            	$phone = $_POST['phone'];
            	// Recupero la password criptata dal form di inserimento.
            	$password = $_POST['p'];
            	// Crea una chiave casuale
            	$random_salt = hash('sha512', uniqid(mt_rand(1, mt_getrandmax()), true));
            	// Crea una password usando la chiave appena creata.
            	$password = hash('sha512', $password.$random_salt);
            	// Inserisci a questo punto il codice SQL per eseguire la INSERT nel tuo database
            	// Assicurati di usare statement SQL 'prepared'.
            	if ($insert_stmt = $conn->prepare("INSERT INTO users (username, email, password, salt, address, name, lastname, phone) VALUES (?, ?, ?, ?, ?, ?, ?, ?)")) {
            		$insert_stmt->bind_param('ssssssss', $username, $email, $password, $random_salt, $address, $name, $lastname, $phone);
            		// Esegui la query ottenuta.
            		$insert_stmt->execute();

                $reg = 1;
              }
        	  } else {
              $reg = 2;
            }
          }
        }
        ?>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=0">
        <title>Cesena Food</title>
        <?php include 'include.php'; ?>
        <link rel="stylesheet" href="JS/CSS/style.css">
        <script>
          function checkpsw(form, password, password2) {

            if (password.value.length > 0) {
              if (password.value == password2.value) {
                password2.value = "nyaa";
                formhash(form, password);
              } else {
                alert("Password non corrispondenti");
              }
            } else {
            }
          }
        </script>
     </head>
   <body>
     <?php include 'PHP/cart.php'; ?>
     <?php include 'PHP/header.php'; ?>
     <?php include 'PHP/hamburger.php'; ?>
     <?php
     if(login_check($conn) == true) {
       header('Location: ./account.php');
     }?>
     <div id="container">
       <div id="main">
         <section>
               <div class="container container-register">
                 <?php include 'PHP/cart-button.php'?>
                  <h1 class="display-4 margin50" style="text-align: center;">Registrati</h1>
                  <?php
                     if ($reg == 1) { ?>
                  <div class="alert alert-success" role="alert">
                     Registrazione effettuata! <a href="login.php">Accedi qui</a>
                  </div>
                  <?php }

                     if ($reg == 2) { ?>
                  <div class="alert alert-danger" role="alert">
                     E-mail già utilizzata!
                  </div>
                  <?php } ?>
                  <form action="signup.php" method="post" name="signup_form">
                     <div class="form-row align-items-center">
                        <div class="col-sm-3 my-1">
                           <label for="username">Username</label>
                           <input type="text" class="form-control" id="username" name="username" required/><br />
                        </div>
                        <div class="col my-1">
                           <label for="email">Email</label>
                           <input type="email" class="form-control" id="email" name="email" required/><br />
                        </div>
                     </div>
                     <div class="form-row align-items-center">
                       <div class="col-sm-6 my-1">
                         <label for="password">Password</label>
                         <input type="password" class="form-control" id="password" name="password" required/><br />
                       </div>
                       <div class="col-sm-6 my-1">
                         <label for="password2">Reinserisci password</label>
                         <input type="password" class="form-control" id="password2" name="password2" required/><br />
                       </div>
                     </div>
                     <div class="form-row align-items-center">
                        <div class="col-sm-8 my-1">
                           <label for="address">Indirizzo</label>
                           <input type="text" class="form-control" id="address" name="address" required/><br />
                        </div>
                        <div class="col-sm-4 my-1">
                           <label for="phone">Telefono</label>
                           <input type="tel" class="form-control" id="phone" name="phone" required/><br />
                        </div>
                     </div>
                     <div class="form-row align-items-center">
                        <div class="col-sm-6 my-1">
                           <label for="firstname">Nome</label>
                           <input type="text" class="form-control" id="firstname" name="name" required/><br />
                        </div>
                        <div class="col-sm-6 my-1">
                           <label for="lastname">Cognome</label>
                           <input type="text" class="form-control" id="lastname" name="lastname" required/><br />
                        </div>
                     </div>
                     <div class="form-check margin20">
                        <input class="form-check-input" type="checkbox" value="" id="defaultCheck1" required>
                        <label class="form-check-label" for="defaultCheck1">
                        Accetto <a href="#">l'Informativa sulla Privacy</a>
                        </label>
                     </div>
                     <input type="submit" class="btn btn-primary fullsize" value="Registrati" name="button" onclick="checkpsw(this.form, this.form.password, this.form.password2);" />
                  </form>
               </div>
             </section>
       </div>
     </div>


      <div class="lightbox js-lightbox js-toggle-cart"></div>
      <?php include'PHP/footer.php' ?>

      <script src="JS/menu.js"></script>
      <script src="JS/cart.js"></script>

   </body>
</html>

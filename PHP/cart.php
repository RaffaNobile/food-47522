<aside class="cart js-cart">
   <div class="cart__header">
      <h1 class="cart__title">Carrello</h1>
      <p class="cart__text">
         <a class="fa fa-times-circle js-toggle-cart" href="#" id="chiudi-carrello" title="Close cart">
         </a>
      </p>
   </div>
   <div class="cart__products">

<div id="shopping-cart-results">
</div>

   </div>
</aside>

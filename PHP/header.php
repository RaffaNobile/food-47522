<?php
   if(login_check($conn) == true) {
     $user_id = $_SESSION['id'];

     if ($stmt = $conn->prepare("SELECT admin, username FROM users WHERE id = ?")) {
       $stmt->bind_param('i', $user_id);
       $stmt->execute(); // esegue la query appena creata.
       $stmt->store_result();
       $stmt->bind_result($admin, $user); // recupera il risultato della query e lo memorizza nelle relative variabili.
       $stmt->fetch();
       $stmt->close();
   ?>
<header>


   <div class="mobile-nav">
      <span class="menu-left"><i class="fa fa-navicon"></i></span>
      <div class="counter">

      </div>
      <div class="logo">
        <a href="index.php"><img src="JS/CSS/logo.png" alt="City Food"></a>
      </div>

        <span class="menu-right js-toggle-cart"><span class="badge" id="cart-badge">    <a  class="cart-counter" id="cart-info" title="View Cart">
            <span class="cart-item" id="cart-container"><?php
               if(isset($_SESSION["products"])){
                 echo count($_SESSION["products"]);
               } else {
                 echo 0;
               }
               ?></span>
            </a></span><i class="fa fa-shopping-cart" ></i></span>

   </div>
   <div class="desktop-nav">
   <nav class="navbar navbar-expand-lg navbar-light bg-faded">
     <div class="logo">
      <a href="index.php"><img src="JS/CSS/logo.png" alt="City Food"></a>
     </div>
      <ul class="navbar-nav mr-auto">
         <li class="nav-item">
            <a class="nav-link" href="#">Dove siamo</a>
         </li>
         <li class="nav-item">
            <a class="nav-link" href="#">Contatti</a>
         </li>
      </ul>
      <p data-newOrder="4" id="bubble"></p>
      <ul class="navbar-nav">
         <li class="nav-item">
            <a class="nav-link" href="PHP/logout.php"><i class="fa fa-user"></i>   Esci</a>
         </li>
         <li class="nav-item">
            <a class="nav-link" href="account.php"><i class="fa fa-user"></i>   Ciao,  <?php echo $user ?> </a>
         </li>
      </ul>
   </nav>
   </div>
</header>
<?php
}}  else {
       ?>
<header>
   <div class="mobile-nav">
      <span class="menu-left"><i class="fa fa-navicon"></i></span>
      <a href="index.php"><img src="JS/CSS/logo.png" alt="City Food"></a>
      <span class="menu-right js-toggle-cart"><span class="badge" id="cart-badge">    <a  class="cart-counter" id="cart-info" title="View Cart">
          <span class="cart-item" id="cart-container"><?php
             if(isset($_SESSION["products"])){
               echo count($_SESSION["products"]);
             } else {
               echo 0;
             }
             ?></span>
          </a></span><i class="fa fa-shopping-cart" ></i></span>
   </div>
   <div class="desktop-nav">
   <nav class="navbar navbar-expand-lg navbar-light bg-faded">
      <a href="index.php"><img src="JS/CSS/logo.png" alt="City Food"></a>
      <ul class="navbar-nav mr-auto">
         <li class="nav-item">
            <a class="nav-link" href="#">Dove siamo</a>
         </li>
         <li class="nav-item">
            <a class="nav-link" href="#">Contatti</a>
         </li>
      </ul>
      <ul class="navbar-nav">
         <li class="nav-item">
            <a class="nav-link" href="login.php"><i class="fa fa-user"></i>   Accedi</a>
         </li>
         <li class="nav-item">
            <a class="nav-link" href="signup.php"><i class="fa fa-sign-in"></i>   Registrati</a>
         </li>
      </ul>
   </nav>
   </div>
</header>
<?php
   }
       ?>

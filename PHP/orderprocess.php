<?php
include 'db_connect.php';
include 'functions.php';

sec_session_start();

if(login_check($conn) == true) {
  $user_id = $_SESSION['id'];
  $shipping_cost = 1.5;
  $total = 0;

  foreach($_SESSION["products"] as $product){
    $product_qty = $product["product_qty"];
    $product_price = $product["price"];
    $subtotal = ($product_price * $product_qty);
    $total = ($total + $subtotal);
  }

  $grand_total = $total + $shipping_cost;

  if ($stmt = $conn->prepare("SELECT address FROM users WHERE id = ?")) {
          $stmt->bind_param('i', $user_id);
          $stmt->execute(); // esegue la query appena creata.
          $stmt->store_result();
          $stmt->bind_result($address); // recupera il risultato della query e lo memorizza nelle relative variabili.
          $stmt->fetch();
          $stmt->close();

  if ($insert_stmt = $conn->prepare("INSERT INTO orders(user_id, price, date, address, seen, status) VALUES (?, ?, NOW(), ?, 0, 0)")) {
    $insert_stmt->bind_param('iss', $user_id, $grand_total ,$address);
    $insert_stmt->execute();
    }
  }

  if ($stmt = $conn->prepare("SELECT id FROM orders WHERE user_id = ? ORDER BY id DESC LIMIT 1")) {
          $stmt->bind_param('i', $user_id);
          $stmt->execute(); // esegue la query appena creata.
          $stmt->store_result();
          $stmt->bind_result($order_id); // recupera il risultato della query e lo memorizza nelle relative variabili.
          $stmt->fetch();
          $stmt->close();

    foreach($_SESSION["products"] as $product){
      $product_qty = $product["product_qty"];
      $product_price = $product["price"];
      $product_code = $product["product_code"];

      if ($stmt = $conn->prepare("SELECT id FROM food WHERE product_code = ?")) {
              $stmt->bind_param('s', $product_code);
              $stmt->execute(); // esegue la query appena creata.
              $stmt->store_result();
              $stmt->bind_result($food_id); // recupera il risultato della query e lo memorizza nelle relative variabili.
              $stmt->fetch();
              $stmt->close();

      if ($insert_stmt = $conn->prepare("INSERT INTO details(order_id, food_id, price, quantity) VALUES (?, ?, ?, ?)")) {
        $insert_stmt->bind_param('iids', $order_id, $food_id, $product_price, $product_qty);
        $insert_stmt->execute();
        }
      }
    }
  }

  unset($_SESSION['products']);

  header('Location: ../success.php');

} else {
  header('Location: ../login.php');
}
?>

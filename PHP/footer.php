<footer id='footer'>
    <div class="footer-info">
        <div class="container">
            <div class="row">
                <div class="col-sm">
                    <h2>Contatti</h2>
                    <li><a href="#" target="">Informazioni su City Food</a></li>
                    <li><a href="#" target="">Domande frequenti</a></li>
                    <li><a href="#" target="">Allergeni</a></li>
                </div>
                <div class="col-sm">
                    <h2>Chi siamo</h2>
                    <ul>
                        <li><a href="#" target="">Lavora con noi</a></li>
                        <li><a href="#" target="">Informativa sulla Privacy</a></li>
                        <li><a href="#" target="">Termini e Condizioni</a></li>
                        <li><a href="cookie-policy.php" target="">Cookie Policy</a></li>


                    </ul>
                </div>
            
                <div class="col-sm" id="methods">
                  <h2>Metodi di pagamento</h2>

                    <a href="#" class="fa fa-cc-paypal" title="Paypal"></a>
                    <a href="#" class="fa fa-credit-card" title="carta di credito"></a>
                    <a href="#" class="fa fa-euro" title="Contanti"></a>

                </div>

            </div>
        </div>
    </div>

    <div class="social-bar">
        <a href="#" class="fa fa-instagram tooltipped" title="instagram"></a>
        <a href="#" class="fa fa-youtube-square tooltipped" title="youtube"></a>
        <a href="#" class="fa fa-facebook-square tooltipped" title="facebook"></a>
        <a href="#" class="fa fa-twitter-square tooltipped" title="twitter"></a>
        <a href="#" class="fa fa-google-plus-square tooltipped" title="google+"></a>
    </div>

    <div class="copyright-bar">

    </div>
</footer>

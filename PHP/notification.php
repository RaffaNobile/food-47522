<?php

  if(login_check($conn) == true) {
    $user_id = $_SESSION['id'];

    if ($stmt = $conn->prepare("SELECT admin FROM users WHERE id = ?")) {
      $stmt->bind_param('i', $user_id);
      $stmt->execute(); // esegue la query appena creata.
      $stmt->store_result();
      $stmt->bind_result($admin); // recupera il risultato della query e lo memorizza nelle relative variabili.
      $stmt->fetch();
      $stmt->close();

      if ($admin == 1) {
        if ($stmt = $conn->prepare("SELECT COUNT(id) FROM orders WHERE seen = 0")) {
          $stmt->execute(); // esegue la query appena creata.
          $stmt->store_result();
          $stmt->bind_result($orders); // recupera il risultato della query e lo memorizza nelle relative variabili.
          $stmt->fetch();
          $stmt->close();

          $test = $orders;
        }
      } else {
        if ($stmt = $conn->prepare("SELECT COUNT(id) FROM orders WHERE user_id = ? AND status = 1")) {
          $stmt->bind_param('i', $user_id);
          $stmt->execute(); // esegue la query appena creata.
          $stmt->store_result();
          $stmt->bind_result($orders); // recupera il risultato della query e lo memorizza nelle relative variabili.
          $stmt->fetch();
          $stmt->close();

        $test =  $orders;
      }
    }
  }
}
 ?>

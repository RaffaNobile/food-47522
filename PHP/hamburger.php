<?php

 if(login_check($conn) == true) {
   $user_id = $_SESSION['id'];

   if ($stmt = $conn->prepare("SELECT username FROM users WHERE id = ?")) {
                   $stmt->bind_param('i', $user_id);
                   // Esegui la query ottenuta.
                   $stmt->execute();
                   $stmt->bind_result($username);
                   $stmt->fetch();
                   $stmt->close();
   ?>

<div id="menu-left">
  <div class="menu-header">
     <h1 class="cart__title"> Ciao,  <?php echo $username ?></h1>
  </div>
  <div class="content">
<h5>Categorie</h5>
    <ul>
      <li><a href="index.php"><i class="fa fa-home"></i> HOME</a></li>
      <li><a href="food.php"><i class="fa fa-user"></i> MENU </a></li>
    </ul>
    <h5>Impostazioni</h5>
    <ul>
      <li><a href="account.php"><i class="fa fa-user animated"></i> I miei ordini</a></li>
      <li><a href="PHP/logout.php"><i class="fa fa-user animated"></i> LOGUOT</a></li>
    </ul>
  </div>

  <div class="menu-footer">
     <p class="cart__text">
     </p>
  </div>
</div>

<?php
}} else {
  ?>

  <div id="menu-left">
    <div class="menu-header">
       <h1 class="cart__title"> Benvenuto su City Food</h1>
    </div>
  <div class="content">
    <h5>Categorie</h5>
    <ul>
      <li><a href="index.php"><i class="fa fa-home"></i> HOME</a></li>
      <li><a href="login.php"><i class="fa fa-user"></i> MENU</a></li>
    </ul>
    <h5>Impostazioni</h5>
    <ul>
      <li><a href="login.php"><i class="fa fa-user"></i> ACCEDI</a></li>
      <li><a href="signup.php"><i class="fa fa-sign-in"></i> REGISTRATI</a></li>
    </ul>

  </div>

    <div class="menu-footer">
       <p class="cart__text">
       </p>
    </div>
  </div>

<?php
}
 ?>

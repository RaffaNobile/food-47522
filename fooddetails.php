

<?php $currency = '	&euro; '; ?>
<!DOCTYPE html>
<html lang="it">
   <head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=0">
      <title>Cesena Food</title>
      <?php include 'include.php' ?>
      <link rel="stylesheet" href="JS/CSS/style.css">
   </head>
   <body>
      <?php
         include 'PHP/db_connect.php';
         include 'PHP/functions.php';
          sec_session_start();
         include 'PHP/cart.php' ;
         include 'PHP/header.php' ;
         include 'PHP/hamburger.php' ;?>
      <div class="lightbox js-lightbox js-toggle-cart"></div>
      <div id="container">
         <div id="main">
    <?php include 'PHP/cart-button.php'?>
            <div class="container container-register container-login col-sm-8 col-md-6 col-lg-4" id="menu-container">
               <?php
                  if (isset($_GET['category'])) {
                      $category=$_GET['category'];

                      if ($stmt = $conn->prepare("SELECT product_code, name, price, description FROM food WHERE category = ?")) {
                              $stmt->bind_param('s', $category);
                              $stmt->execute(); // esegue la query appena creata.
                              $stmt->store_result();
                              $stmt->bind_result($id_food, $name, $price, $description); // recupera il risultato della query e lo memorizza nelle relative variabili.
                              ?>
                <div class="back-button">
                  <button class="btn btn-danger" onclick="goBack()"><i class="material-icons" style="vertical-align: sub; font-size: 20px;">chevron_left</i>Indietro</button>
                  <script>
                     function goBack() {
                        window.history.back();
                     }
                  </script>
                </div>

               <h3 class="text-center"><?php echo $category ?></h3>
               <table class="table table-striped text-center">
                  <thead>
                     <tr>
                        <th  scope="col">Nome</th>
                        <th  scope="col">Prezzo</th>
                        <th  scope="col">Quantità</th>
                        <th  scope="col"></th>
                     </tr>
                  </thead>
               </table>
               <?php
                  while ($stmt->fetch()) {
                  ?>
               <form class="product-form ">
                  <table class="table table-striped">
                     <caption class="text-center" align="bottom">Ingredienti: <?php echo $description ?></caption>
                     <tbody>
                        <tr>
                           <td class="text-center"><?php echo $name; ?></td>
                           <td class="text-center"><?php echo $currency; echo $price; ?></td>
                           <td class="text-center">
                              <select name="product_qty">
                                 <option value="1">1</option>
                                 <option value="2">2</option>
                                 <option value="3">3</option>
                                 <option value="4">4</option>
                                 <option value="5">5</option>
                              </select>
                           </td>
                           <td class="text-center">
                              <input name="product_code" type="hidden" value="<?php echo $id_food ?>">
                              <button type="submit" class="btn-primary" id="add-button"> <i class="fa fa-plus"></i></button>
                           </td>
                        </tr>
                     </tbody>
                  </table>
               </form>
               <?php }}} ?>
            </div>
         </div>
      </div>
      <script src="JS/menu.js"></script>
      <script src="JS/cart.js"></script>
      <?php include 'PHP/footer.php' ?>
   </body>
</html>

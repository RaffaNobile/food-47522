<!doctype html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=0">
    <title>Payment</title>
    <?php include 'include.php' ?>
    <link rel="stylesheet" href="JS/CSS/style.css">
  </head>

  <body>
    <?php include 'PHP/db_connect.php';?>
    <?php include 'PHP/functions.php';?>
    <?php
       sec_session_start();
       ?>
    <?php include 'PHP/cart.php' ?>
    <?php include 'PHP/header.php' ?>
    <?php include 'PHP/hamburger.php' ?>
    <div class="lightbox js-lightbox js-toggle-cart"></div>
    <div id="container">
       <div id="main">
         <div class="container container-register">
           <form class="needs-validation" novalidate>
                 <div class="d-block my-3">
                   <div class="custom-control custom-radio">
                     <input id="credit" name="paymentMethod" type="radio" class="custom-control-input" checked required>
                     <label class="custom-control-label" for="credit">Visa</label>
                   </div>
                   <div class="custom-control custom-radio">
                     <input id="debit" name="paymentMethod" type="radio" class="custom-control-input" required>
                     <label class="custom-control-label" for="debit">MasterCard</label>
                   </div>
                   <div class="custom-control custom-radio">
                     <input id="paypal" name="paymentMethod" type="radio" class="custom-control-input" required>
                     <label class="custom-control-label" for="paypal">Paypal</label>
                   </div>
                 </div>
                 <div class="row">
                   <div class="col-md-6 mb-3">
                     <label for="cc-name">Intestatario</label>
                     <input type="text" class="form-control" id="cc-name" placeholder="" required>
                     <small class="text-muted">Nome intestatario</small>
                     <div class="invalid-feedback">
                      Nome necessario
                     </div>
                   </div>
                   <div class="col-md-6 mb-3">
                     <label for="cc-number">Numero carta</label>
                     <input type="text" class="form-control" id="cc-number" placeholder="" required>
                     <div class="invalid-feedback">
                       Numero carta necessario
                     </div>
                   </div>
                 </div>
                 <div class="row">
                   <div class="col-md-3 mb-3">
                     <label for="cc-expiration">Scadenza</label>
                     <input type="text" class="form-control" id="cc-expiration" placeholder="" required>
                     <div class="invalid-feedback">
                       Scadenza necessaria
                     </div>
                   </div>
                   <div class="col-md-3 mb-3">
                     <label for="cc-expiration">CVV</label>
                     <input type="text" class="form-control" id="cc-cvv" placeholder="" required>
                     <div class="invalid-feedback">
                       Codice sicurezza necessario
                     </div>
                   </div>
                 </div>
                 <hr class="mb-4">
                 <button class="btn btn-primary btn-lg btn-block" type="submit" formaction="PHP/orderprocess.php">Continua</button>
               </form>

           </div>
         </div>
       </div>

    <script>

      (function() {
        'use strict';
        window.addEventListener('load', function() {

          var forms = document.getElementsByClassName('needs-validation');

          var validation = Array.prototype.filter.call(forms, function(form) {
            form.addEventListener('submit', function(event) {
              if (form.checkValidity() === false) {
                event.preventDefault();
                event.stopPropagation();
              }
              form.classList.add('was-validated');
            }, false);
          });
        }, false);
      })();
    </script>
    <script src="JS/menu.js"></script>
    <script src="JS/cart.js"></script>
    <?php include 'PHP/footer.php' ?>
  </body>
</html>

<!DOCTYPE html>
<html lang="it">
   <head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=0">
      <title>City Food</title>
      <?php include 'include.php' ?>
      <link rel="stylesheet" href="JS/CSS/style.css">
   </head>
   <body id="main-page">
      <?php include 'PHP/db_connect.php';?>
      <?php include 'PHP/functions.php';?>
      <?php
         sec_session_start();
         ?>
      <?php include 'PHP/cart.php' ?>
      <?php include 'PHP/header.php' ?>
      <?php include 'PHP/hamburger.php' ?>


      <div class="lightbox js-lightbox js-toggle-cart"></div>
      <div id="container">
         <div id="main">
           <?php include 'PHP/cart-button.php'?>
            <div class="banner">

               <h2>HAI FAME? </h2>
               <h5>Ordina subito e soddisfa i tuoi bisogni </h5>
               <div class="banner-button">
                  <a  type="button" class="btn btn-primary btn-lg" href="food.php">VAI AL MENU </a>
               </div>
            </div>
         </div>
      </div>
      <script src="JS/menu.js"></script>
      <script src="JS/cart.js"></script>
      <?php include 'PHP/footer.php' ?>
   </body>
</html>

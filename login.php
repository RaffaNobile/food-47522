<!DOCTYPE html>
<html lang="it">
   <head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=0">
      <title>Cesena Food</title>
      <?php include 'include.php'; ?>
      <link rel="stylesheet" href="JS/CSS/style.css">
   </head>
   <body>
     <?php include 'PHP/db_connect.php';?>
     <?php include 'PHP/functions.php';?>
     <?php sec_session_start(); ?>
     <?php include 'PHP/cart.php'; ?>
     <?php include 'PHP/header.php'; ?>
     <?php include 'PHP/hamburger.php'; ?>

     <?php

     if(login_check($conn) == true) {
       header('Location: ./account.php');
     }?>
     <div id="container">
       <div id="main">
         <div class="container container-register container-login col-sm-8 col-md-6 col-lg-4">
           <?php include 'PHP/cart-button.php'?>
            <h1 class="display-4 margin50" style="text-align: center;">Login</h1>
            <?php
               if(isset($_GET['error'])) { ?>
            <div class="alert alert-danger" role="alert">
               Email o password errati!
            </div>
            <?php } ?>
            <form action="PHP/process_login.php" method="post" name="login_form">
               <label for="email">Email</label>
               <input type="text" class="form-control" id="email" name="email" /><br />
               <label for="password">Password</label>
               <input type="password" class="form-control" name="passwd" id="password"/><br />
               <input type="submit" class="btn btn-primary fullsize" value="Login" onclick="formhash(this.form, this.form.password);" />
            </form>
         </div>


       </div>
     </div>
     <div class="lightbox js-lightbox js-toggle-cart"></div> <!-- AGGIUNGERE PER FORZA IN OGNI PAGINA -->
       <?php include 'PHP/footer.php' ?>

       <script src="JS/menu.js"></script>
       <script src="JS/cart.js"></script>


   </body>
</html>

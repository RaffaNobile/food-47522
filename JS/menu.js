(function() {
    $('.menu-left').click(function() {
        $('header').toggleClass('active')
        $('.intro').toggleClass('active')
        $('section').toggleClass('active')
        $('#content').toggleClass('active')
        $('#menu-left').toggleClass('active')
        $('#cnt').toggleClass('active')
        $('footer').toggleClass('active')
    })

})()

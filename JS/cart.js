var cartOpen = false;

$('body').on('click', '.js-toggle-cart', toggleCart);


function toggleCart(e) {
  e.preventDefault();
  if(cartOpen) {
    closeCart();
    return;
  }
  openCart();
}

function openCart() {
  cartOpen = true;
  $('body').addClass('open');
}

function closeCart() {
  cartOpen = false;
  $('body').removeClass('open');
}


$(document).ready(function(){
	// update product quantity in cart
    $(".quantity").change(function() {
		 var element = this;
		 setTimeout(function () { update_quantity.call(element) }, 1000);
	});
	function update_quantity() {
		var pcode = $(this).attr("data-code");
		var quantity = $(this).val();
		$(this).parent().parent().fadeOut();
		$.getJSON( "manage_cart.php", {"update_quantity":pcode, "quantity":quantity} , function(data){
	  window.location.reload();
		});
	}

	// add item to cart
	$(".product-form").submit(function(e){
		var form_data = $(this).serialize();

		$.ajax({
			url: "manage_cart.php",
			type: "POST",
			dataType:"json",
			data: form_data
		}).done(function(data){
			$("#cart-container").html(data.products);
       console.log("Oggetto aggiunto");
		})
		e.preventDefault();

	});
  $(".product-form").submit(function(e){
    var form_data = $(this).serialize();

    $.ajax({
      url: "manage_cart.php",
      type: "POST",
      dataType:"json",
      data: form_data
    }).done(function(data){
      $("#cart-container-desktop").html(data.products);
       console.log("Oggetto aggiunto");
    })
    e.preventDefault();

  });
	//Remove items from cart
	$("#shopping-cart-results").on('click', 'a.remove-item', function(e) {
		e.preventDefault();
		var pcode = $(this).attr("data-code");
		$(this).parent().parent().fadeOut();
		$.getJSON( "manage_cart.php", {"remove_code":pcode} , function(data){
			$("#cart-container").html(data.products);
			window.location.reload();
		});
	});
});

//Update cart
$("#ds-sticky-button").click(function(e) { //when user clicks on cart box
		e.preventDefault();
    $("#shopping-cart-results").html('<img src="JS/CSS/img/loader.gif">'); //show loading image
		$("#shopping-cart-results" ).load( "manage_cart.php", {"load_cart":"1"}); //Make ajax request using jQuery Load() & update results
	});
  $(".menu-right").click(function(e) { //when user clicks on cart box
  		e.preventDefault();
      $("#shopping-cart-results").html('<img src="JS/CSS/img/loader.gif">'); //show loading image
  		$("#shopping-cart-results" ).load( "manage_cart.php", {"load_cart":"1"}); //Make ajax request using jQuery Load() & update results
  	});


    // hide
